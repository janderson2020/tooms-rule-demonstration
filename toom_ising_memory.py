# Demonstration of Tooms Rule modified from
# game of life 2d - gumuz
# http://www.gumuz.nl

# pyglet imports
from pyglet.gl import *
from pyglet import window, clock, image
from pyglet.window import key

# standard lib imports
from random import choice, random

grid_size = 50

def get_label(m,fs):
    return pyglet.text.Label(m, 
                             font_name='Times New Roman',
                             font_size=fs,
                             x=win.width//4, y=win.width//1.9,
                             anchor_x='center', anchor_y='center')
    
def get_label2(m,fs):
    return pyglet.text.Label(m, 
                             font_name='Times New Roman',
                             font_size=fs,
                             x=3*win.width//4, y=win.width//1.9,
                             anchor_x='center', anchor_y='center')
    
def get_label3(m,fs):
    return pyglet.text.Label(m, 
                             font_name='Times New Roman',
                             font_size=fs,
                             x=win.width//2, y=win.width//1.73,
                             anchor_x='center', anchor_y='center')


def error(pin):
    if pin > random():
        return True
    return False

def seed_grid():
    # populate the grid at random
    global grid, grid2
    
    for y in range(grid_size):
        for x in range(grid_size):
            grid[y][x] = choice((0,0,0,0,0))
            
    for y in range(grid_size):
        for x in range(grid_size):       
            grid2[y][x] = choice((0,0,0,0,0))

def evolve_grid():
    # evolve the grid
    global grid2, grid
    global p_err
    global q_err
    
    # empty working grid
    working_grid = [[0]*grid_size for i in [0]*grid_size]
    working_grid2 = [[0]*grid_size for i in [0]*grid_size]
    
    Ising_M = 0.0 #Initial value for magnetization
    Tooms_M = 0.0 #Initial value for magnetization
    
    for y in range(grid_size):
        for x in range(grid_size):
            neighbors = [(0, 0), (0, 1), (1, 0), (0, -1), (-1, 0)]
            
            value = 0
            for a,b in neighbors:
                ny, nx = (y+a)%grid_size, (x+b)%grid_size  # perodic boundary conditions
                value += grid2[ny][nx]
            
            if value >= 3:
                if error(p_err):
                    Ising_M -= 1
                    working_grid2[y][x] = 0
                else:
                    Ising_M += 1
                    working_grid2[y][x] = 1
            else: 
                if error(q_err):
                    Ising_M += 1
                    working_grid2[y][x] = 1
                else:
                    Ising_M -= 1
                    working_grid2[y][x] = 0
                
            # count the 2 neighbors (NEC) for Tooms rule
            offsets = [(0, 0), (1, 0), (0, 1)]
            value = 0
            for a,b in offsets:
                ny, nx = (y+a)%grid_size, (x+b)%grid_size  # perodic boundary conditions
                value += grid[ny][nx]
                
            if value in (2,3):
                if error(p_err):
                    Tooms_M -= 1
                    working_grid[y][x] = 0
                else:
                    Tooms_M += 1
                    working_grid[y][x] = 1
            else: 
                if error(q_err):
                    Tooms_M += 1
                    working_grid[y][x] = 1
                else:
                    Tooms_M -= 1
                    working_grid[y][x] = 0
    
    # copy working_grid onto gid
    grid = working_grid
    grid2 = working_grid2
    
    return Ising_M, Tooms_M

def draw_grid():
    # draw the grid squares
    global grid, grid2
    
    # opengl quad-mode
    glBegin(GL_QUADS)
    
    # square vertices
    square_verts = [ (0,0), (10,0), 
                     (10,10), (0,10)]
    
    for y in range(grid_size):
        for x in range(grid_size):
            if grid[y][x]: 
                
                glColor3f(0,5,5)
                for vx,vy in square_verts:
                    vx = vx +10 + (x*10)
                    vy = vy + 10 + (y*10)
                    glVertex2f(vx,vy)
                    
    for y in range(grid_size):
        for x in range(grid_size):
            if grid2[y][x]:
                
                glColor3f(0,5,5)
                for vx,vy in square_verts:
                    vx = vx +10 + (x*10) + 511
                    vy = vy + 10 + (y*10)
                    glVertex2f(vx,vy)
    glEnd()

def draw_arena():
    # draw arena lines
    glColor3f(.5,.5,.5)
    
    # opengl line-mode
    glBegin(GL_LINES)
    
    # arena vertices
    arena_verts = [ (9, 9), (511, 9), (511, 511), 
                    (9, 511), (9, 9) ]
    
    arena2_verts = [ (520, 9), (1022, 9), (1022, 511), 
                    (520, 511), (520, 9) ]
    
    for i in range(len(arena_verts)-1):
        glVertex2f(*arena_verts[i])
        glVertex2f(*arena_verts[i+1])
        
    for i in range(len(arena2_verts)-1):
        glVertex2f(*arena2_verts[i])
        glVertex2f(*arena2_verts[i+1])

    glEnd()
    

def draw_header():
    global header_img
    
    glColor3f(1,1,1)
    header_img.blit(10,630)
    
def draw_help():
    global controls_img
    
    glColor3f(1,1,1)
    controls_img.blit(100,200)

    
# The OpenGL context
win = window.Window(visible=False,width=(2*520),height=(680))
win.set_visible()

# some globals
header_img = image.load('header.png').texture
controls_img = image.load('controls.png').texture

show_help = True
evolving = False
painting = False


fps_limit = 10
clock.set_fps_limit(fps_limit)



# events
def on_key_press(symbol, modifiers):
    global evolving, fps_limit, grid, grid2, show_help
    global p_err
    global q_err
    
    if show_help:
        show_help = False
        return

    if symbol == key.H:
        # show help screen
        show_help = True
    if symbol == key.SPACE:
        # toggle evolving
        evolving = not evolving
    if symbol == key.R:
        # reseed grid
        seed_grid()
    if symbol == key.C:
        # clear grid
        grid = [[0]*grid_size for i in range(grid_size)]
        grid2 = [[0]*grid_size for i in range(grid_size)]
    
    #if symbol == key.UP:
    #    fps_limit += 2
    #    if fps_limit > 40: fps_limit = 40
    #    clock.set_fps_limit(fps_limit)
    #if symbol == key.DOWN:
    #    fps_limit -= 2
    #    if fps_limit < 2: fps_limit = 2
    #    clock.set_fps_limit(fps_limit)
    
    if symbol == key.UP:
        if q_err >= 0.01:
            p_err += 0.01
            q_err -= 0.01
        
    if symbol == key.DOWN:
        if p_err >= 0.01:
            p_err -= 0.01
            q_err += 0.01
        
    if symbol == key.RIGHT:
        p_err += 0.01
        q_err += 0.01
        
    if symbol == key.LEFT:
        if p_err >= 0.01 and q_err >= 0.01:
            p_err -= 0.01
            q_err -= 0.01
        
win.on_key_press = on_key_press

# empty zero-filled grid
grid = [[0]*grid_size for i in range(grid_size)]
grid2 = [[0]*grid_size for i in range(grid_size)]
p_err = 0.0
q_err = 0.0

#Ising_M = 0
#Tooms_M = 0

while not win.has_exit:
    if p_err + q_err == 0:
        h = 0
    else:
        h = (p_err - q_err)/(p_err + q_err) # magnetic field or bias
    t = p_err + q_err # temperature of strength 
    
    win.dispatch_events()
    dt = clock.tick()
    
    # set title framerate
    win.set_caption('Toom\'s Rule vs. Ising model')
    
    
    # clear
    glClear(GL_COLOR_BUFFER_BIT)
    glLoadIdentity()
    
    draw_header()
    draw_arena()
    
    try:
        string = 'Magnetization: ' + str(Tooms_M/grid_size**2)
        string2 = 'Magnetization: ' + str(Ising_M/grid_size**2)
        string3 = 'Temperature = ' + str(t) + '\t\tBias = ' + str(h)
    except:
        string = 'Magnetization: '
        string2 = 'Magnetization: '
        string3 = 'Temperature = ' + '\t\t' + 'Bias = '
    
    if evolving and not painting and not show_help: 
        Ising_M, Tooms_M = evolve_grid()
        string = 'Magnetization: ' + str(Tooms_M/grid_size**2)
        string2 = 'Magnetization: ' + str(Ising_M/grid_size**2)
    
    label = get_label(string, 36)
    label.draw()
    label2 = get_label2(string2, 36)
    label2.draw()
    label3 = get_label3(string3, 36)
    label3.draw()
    draw_grid()
    if show_help: draw_help()

    win.flip()